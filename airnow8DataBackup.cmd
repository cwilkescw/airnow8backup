:: Batch file to create a backup of the MySQL 5.7 database for the Drupal AirNow website
@echo off
cls
echo.
echo.Starting Drupal AirNow MySQL database backup at %DATE% %Time%
echo.

:: get to bin directory first
cd C:\Program Files\MySQL\MySQL Server 5.7\bin\

:: Find current date and time
::cls
::echo Date format = %date%
::echo dd = %date:~7,2%
::echo mm = %date:~4,2%
::echo yyyy = %date:~10,4%
::echo.
::echo Time format = %time%
::SET HOUR=%TIME:~0,2%
::IF "%HOUR:~0,1%" == " " SET HOUR=0%HOUR:~1,1%
::echo hh = %HOUR%
::echo mm = %time:~3,2%
::echo ss = %time:~6,5%  :: %time:~6,5% for only whole seconds

:: if hour is less than 10 AM
SET HOUR=%TIME:~0,2%
IF "%HOUR:~0,1%" == " " SET HOUR=0%HOUR:~1,1%

Set timestamp=%date:~10,4%-%date:~4,2%-%date:~7,2%
:: use this for leas than once a day
:: T%HOUR%%time:~3,2%%time:~6,5%

echo.%timestamp%

:: Build dump
mysqldump -u127_dr -pNewpass1 drupal8 > C:\Users\cwilkes\Documents\data\airnow8Backup\%timestamp%-airnow8DataBackup.sql

:: get back to the data directory
cd C:\Users\cwilkes\Documents\data\airnow8Backup\

:: Push Data backup into the Repository

:: stage all files in directory
git add .
:: build the commit
git commit -m "Add Data %timestamp%"
:: Push it
git push origin master



:: Feedback
echo.
echo.Done!
echo.